<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Tasks</div>

                    <div class="panel-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <table class="table table-striped table-bordered table-condensed">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Start time</th>
                                <th>End Time</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($tasks as $key => $task)

                                <tr class="list-tasks">
                                    <td>{{ $task->name }}</td>
                                    <td>{{ $task->description }}</td>
                                    <td>{{ $task->start_time }}</td>
                                    <td>{{ $task->end_time }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('tasks.show',$task->id) }}">Show</a>
                                        <a class="btn btn-primary" href="{{ route('tasks.edit',$task->id) }}">Edit</a>

                                        <form action="{{ url('tasks/'.$task->id) }}" method="POST" style="display: inline-block">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" id="delete-task-{{ $task->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <a href="{{ route('tasks.create') }}" class="btn btn-success">New Task</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection