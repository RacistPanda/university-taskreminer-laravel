<?php ?>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Role</div>

                    <div class="panel-body">
                        <!-- Display Validation Errors -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <form class="form-horizontal" role="form" method="POST"
                              action="{{ url('tasks/'.$task->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{$task->name}}"
                                           required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Description</label>

                                <div class="col-md-6">
                                    <textarea rows="4" cols="50" name="description" id="description"
                                              class="form-control">{{$task->description}}</textarea>

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('start_time') ? ' has-error' : '' }}">
                                <label for="start_time" class="col-md-4 control-label">Start time</label>

                                <div class="col-md-6">
                                    <input id="start_time" type="text" class="datetimepicker form-control" name="start_time"
                                           value="{{$task->start_time}}"
                                           required autofocus>

                                    @if ($errors->has('start_time'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('start_time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('end_time') ? ' has-error' : '' }}">
                                <label for="end_time" class="col-md-4 control-label">End time</label>

                                <div class="col-md-6">
                                    <input id="end_time" type="text" class="datetimepicker form-control" name="end_time"
                                           value="{{$task->end_time}}"
                                           required autofocus>

                                    @if ($errors->has('end_time'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('end_time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <hr>
                            <h2 style="text-align: center; margin-bottom: 25px;">Notifications</h2>

                            <div class="form-group{{ $errors->has('permissions') ? ' has-error' : '' }}">

                                <div class="col-md-6 col-md-offset-4">
                                    <div class="outer_wrapper_date">
                                        @if(!empty($task->taskTimes[0]))
                                            @foreach($task->taskTimes as $time)
                                                    <div class="wrapper datepick">
                                                        <div class="clearfix">
                                                            <input style="max-width: 90%; float:left" class="datetimepicker form-control" type="text" value="{{$time->time}}" name="notificationDateTime[{{$loop->index}}]" placeholder="Date and time">

                                                        @if ($errors->has('notificationDateTime.$loop->index'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('notificationDateTime.$loop->index') }}</strong>
                                                            </span>
                                                        @endif
                                                            <span id="addNotification" onclick="removeSelectDatepick(this)" title="Remove Notification" class="glyphicon glyphicon-minus" style="cursor: pointer; float:right; margin-top: 10px;"></span>
                                                        </div>
                                                        <?php $iteration = $loop->index; ?>

                                                        @foreach ($notificationMethods as $key => $notificationMethod)

                                                            <?php $checked = false; ?>

                                                            @if(!empty($time->notificationMethods[0]))
                                                                @foreach($time->notificationMethods as $selectedNotficitionMethod)
                                                                    @if($notificationMethod->id == $selectedNotficitionMethod->id)
                                                                        <?php $checked = true; ?>
                                                                        @break
                                                                    @endif
                                                                @endforeach
                                                            @endif


                                                            <span class="select">
                                                                <input type="checkbox" {{ $checked ? "checked" : "" }} class="notificationMethod" value="{{$notificationMethod->id}}" name="notificationMethod[{{$iteration}}][]"> {{$notificationMethod->name}}
                                                            </span>
                                                        @endforeach


                                                        
                                                    </div>
                                                
                                            @endforeach
                                        @else
                                            <div class="wrapper datepick">
                                                <input class="datetimepicker form-control" type="text" name="notificationDateTime[0]" placeholder="Date and time">
                                                @foreach ($notificationMethods as $key => $notificationMethod)
                                                    <span class="select">
                                                        <input type="checkbox" class="notificationMethod" value="{{$notificationMethod->id}}" name="notificationMethod[0][]"> {{$notificationMethod->name}}
                                                    </span>
                                                @endforeach

                                                @if ($errors->has('notificationMethod'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('notificationMethod') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                    <div style="text-align: right;">
                                        <span id="addNotification" onclick="addSelectDatepick()" style="cursor: pointer;" class="glyphicon glyphicon-plus" title="Add Notification"></span>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>

                                    <a class="btn btn-link" href="{{ url('back/roles') }}">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection