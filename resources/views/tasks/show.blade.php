<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Task Info</div>

                    <div class="panel-body">


                        <div class="form-group clearfix">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            {{ $task->name }}
                        </div>


                        <div class="form-group clearfix">
                            <label for="name" class="col-md-4 control-label">Description</label>
                            {{ $task->description }}
                        </div>

                        <div class="form-group clearfix">
                            <label for="name" class="col-md-4 control-label">Start time</label>
                            {{ $task->start_time }}
                        </div>

                        <div class="form-group clearfix">
                            <label for="name" class="col-md-4 control-label">End time</label>
                            {{ $task->end_time }}
                        </div>

                        <hr>
                        <h2 style="text-align: center; margin-bottom: 25px;">Notifications</h2>

                        <div class="form-group clearfix">
                            @if(!empty($task->taskTimes[0]))
                                <div class="col-md-12 control-label">
                                    @foreach($task->taskTimes as $time)
                                        @if(!$loop->first)
                                            <hr style="margin: 10px auto; max-width: 50%">
                                        @endif
                                        <div class="clearfix" style="text-align: center">
                                            <label class="label label-success" style="margin-right: 5px; font-size: 14px">{{ $time->time }}</label>
                                        
                                            @if(!empty($time->notificationMethods))
                                                @foreach($time->notificationMethods as $notificationMethod)
                                                    <label class="label label-success" style="margin-right: 5px; font-size: 14px">{{ $notificationMethod->name }}</label>
                                                @endforeach
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            @else
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection