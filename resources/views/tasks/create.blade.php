<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create New Task</div>

                    <div class="panel-body">
                        <!-- Display Validation Errors -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('tasks') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                           required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Description</label>

                                <div class="col-md-6">
                                    <textarea rows="4" cols="50" name="description" id="description" class="form-control">{{ old('description') }}</textarea>

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('start_time') ? ' has-error' : '' }}">
                                <label for="start_time" class="col-md-4 control-label">Start Time</label>

                                <div class="col-md-6">
                                     <input class="datetimepicker form-control" type="text" name="start_time" >

                                    @if ($errors->has('start_time'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('start_time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                             <div class="form-group{{ $errors->has('end_time') ? ' has-error' : '' }}">
                                <label for="end_time" class="col-md-4 control-label">End Time</label>

                                <div class="col-md-6">
                                     <input class="datetimepicker form-control" type="text" name="end_time" >

                                    @if ($errors->has('end_time'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('end_time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <h2 style="text-align: center; margin-bottom: 25px;">Notifications</h2>

                            <div class="form-group{{ $errors->has('notificationMethod') ? ' has-error' : '' }}">

                                <div class="col-md-6 col-md-offset-4">
                                    <div class="outer_wrapper_date">
                                        <div class="wrapper datepick">
                                            <div class="clearfix">
                                                <input style="max-width: 90%; float:left" class="datetimepicker form-control" type="text" name="notificationDateTime[0]" placeholder="Date and time">
                                                <span id="addNotification" onclick="removeSelectDatepick(this)" title="Remove Notification" class="glyphicon glyphicon-minus" style="cursor: pointer; float:right; margin-top: 10px;"></span>
                                            </div>
                                                @foreach ($notificationMethods as $key => $notificationMethod)
                                                    <span class="select">
                                                        <input type="checkbox" class="notificationMethod" value="{{$notificationMethod->id}}" name="notificationMethod[0][]"> {{$notificationMethod->name}}
                                                    </span>
                                                @endforeach

                                                @if ($errors->has('notificationMethod'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('notificationMethod') }}</strong>
                                                </span>
                                                @endif
                                            
                                        </div>
                                    </div>
                                    <div style="text-align: right;">
                                        <span id="addNotification" onclick="addSelectDatepick()" style="cursor: pointer;" class="glyphicon glyphicon-plus" title="Add Notification"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>

                                    <a class="btn btn-link" href="{{ url('tasks') }}">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection