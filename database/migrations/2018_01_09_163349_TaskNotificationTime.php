<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TaskNotificationTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::create('task_times', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('task_id')->unsigned();
            $table->dateTime('time');
            $table->timestamps();

            $table->foreign('task_id')->references('id')->on('tasks')
                ->onUpdate('cascade')->onDelete('cascade');
        });

         Schema::create('task_time_notification_method', function (Blueprint $table) {
            $table->integer('task_time_id')->unsigned();
            $table->integer('notification_method_id')->unsigned();

            $table->primary(['task_time_id', 'notification_method_id'], 'task_time_notification_method_primary');

            $table->foreign('task_time_id')->references('id')->on('task_times')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('notification_method_id')->references('id')->on('notification_methods')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('task_times');
        Schema::dropIfExists('task_notification_method');
    }
}
