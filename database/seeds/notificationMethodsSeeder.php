<?php

use Illuminate\Database\Seeder;

class notificationMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('notification_methods')->delete();

        DB::table('notification_methods')->insert([
        	'type' => 'email',
            'name' => 'Email',
        ]);

        DB::table('notification_methods')->insert([
        	'type' => 'sms',
            'name' => 'SMS',
        ]);
    }
}
