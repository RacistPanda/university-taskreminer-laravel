<?php

use Illuminate\Database\Seeder;

class statusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('status')->delete();

        DB::table('status')->insert([
        	'type' => 'notified',
            'name' => 'Notified',
        ]);

        DB::table('status')->insert([
        	'type' => 'pending',
            'name' => 'Pending',
        ]);
    }
}
