<?php

use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->delete();

        date_default_timezone_set('Europe/Vilnius');

        $user = DB::table('users')->first();
        //$status = DB::table('status')->where('type', 'pending')->first();

        DB::table('tasks')->insert([
        	'user_id' => $user->id,
            'name' => str_random(8),
            'description' => str_random(20),
            'start_time' => date("Y-m-d H:i", strtotime('+20 minutes')),
            'end_time' => date("Y-m-d H:i", strtotime('+180 minutes')),
            //'status_id' => $status->id,
        ]);
    }
}
