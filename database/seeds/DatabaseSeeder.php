<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        //$this->call(statusSeeder::class);
        $this->call(notificationMethodsSeeder::class);
        $this->call(TaskSeeder::class);
        $this->call(taskNotificationMethodSeeder::class);
    }
}
