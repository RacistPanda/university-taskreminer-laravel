<?php

use Illuminate\Database\Seeder;

class taskNotificationMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('task_notification_method')->delete();

        $task = DB::table('tasks')->first();
        $notificationMethod = DB::table('notification_methods')->first();

        DB::table('task_notification_method')->insert([
        	'task_id' => $task->id,
        	'notification_method_id' => $notificationMethod->id,
        ]);
    }
}
