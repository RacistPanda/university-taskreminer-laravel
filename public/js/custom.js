initiateDateTimePicker();

function addSelectDatepick(){
	
	var count = $('.panel .datepick').length;

	$('.panel .datepick:last').clone().appendTo('.panel .outer_wrapper_date');
	$('.panel .datepick:last').find('.datetimepicker').attr('name', 'notificationDateTime['+count+']').val('');
	$('.panel .datepick:last').find('.notificationMethod').attr('name', 'notificationMethod['+count+'][]').removeAttr('checked');

	if(!count){
		$('.panel .outer_wrapper_date').append('<div class="wrapper datepick"><div class="clearfix"><input type="text" value="" name="notificationDateTime[0]" placeholder="Date and time" class="datetimepicker form-control" style="max-width: 90%; float: left;"> <span id="addNotification" onclick="removeSelectDatepick(this)" title="Remove Notification" class="glyphicon glyphicon-minus" style="cursor: pointer; float: right; margin-top: 10px;"></span></div> <span class="select"><input type="checkbox" value="1" name="notificationMethod[0][]" class="notificationMethod"> Email</span> <span class="select"><input type="checkbox" value="2" name="notificationMethod[0][]" class="notificationMethod"> SMS</span></div>');
	}

    initiateDateTimePicker();
}

function removeSelectDatepick(test){
	$(test).closest('.panel .datepick').remove();
};

function initiateDateTimePicker(){

	$(function () {
        $('.datetimepicker').datetimepicker({
            format : 'YYYY-MM-DD HH:mm'
        });
    });
}