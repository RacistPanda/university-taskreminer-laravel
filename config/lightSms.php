<?php
return [

	'SmsLogin' => env('SMS_LOGIN', ''),
	'SmsApiKey' => env('SMS_API_KEY', ''),
	'SmsSender' => env('SMS_SENDER', 'smstest'),
	'SmsText' => 'default text',
	'SmsSend' => env('SMS_SEND', 0),
];