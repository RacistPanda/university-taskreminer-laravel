<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
    	'user_id', 'name', 'description', 'start_time', 'end_time', 'status_id', 'notified'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function status(){
    	return $this->belongsTo('App\Status');
    }

    public function taskTimes(){
    	return $this->hasMany('App\TaskTime');
    }
}
