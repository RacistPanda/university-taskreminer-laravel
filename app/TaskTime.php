<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskTime extends Model
{
    protected $fillable = [
    	'task_id', 'time'
    ];

    public function notificationMethods(){

    	return $this->belongsToMany('App\NotificationMethod', 'task_time_notification_method');
    }

    public function task(){

    	return $this->belongsTo('App\Task');
    }
}
