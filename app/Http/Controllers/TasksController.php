<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\NotificationMethod;
use App\Status;
use App\Task;
use App\TaskTime;

use Auth;
use Validator;
use Log;


class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        $tasks = Auth::user()->tasks()->orderBy('id','DESC')->paginate(30);

        return view('tasks.index', compact('tasks'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        
        $notificationMethods = NotificationMethod::get();

        return view('tasks.create', compact('notificationMethods'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $request->validate([
            'name' => 'required|max:255',
            'start_time' => 'date_format:"Y-m-d H:i"|required|before:end_time',
            'end_time' => 'date_format:"Y-m-d H:i"|required|after:start_time',
            'notificationDateTime.*' => 'date_format:"Y-m-d H:i|nullable|after:today',
            'notificationMethod.*' => 'exists:notification_methods,id'
        ]);

        $user = Auth::user();

        $task = $this->storeTask($request, $user);

        foreach($request->notificationDateTime as $key => $notificationDateTime){

            if(!$notificationDateTime) continue;

            $taskTime = $task->taskTimes()->save(
                new TaskTime([
                    'task_id' => $task->id,
                    'time' => $notificationDateTime
                ])
            );

            if($notificationMethod = $request->notificationMethod[$key]){

                $taskTime->notificationMethods()->attach($notificationMethod);
            };
        }

        return redirect('tasks')->with('success','Task succefully created');
    }

    private function storeTask($request, $user){

        $task = new Task();
        $task->user_id = $user->id;
        $task->name = $request->get('name');
        $task->description = $request->get('description');
        $task->start_time = $request->get('start_time');
        $task->end_time = $request->get('end_time');
        $task->save();

        return $task;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){

        $task = Task::where('id', $id)->with('taskTimes.notificationMethods')->first();

        return view('tasks.show', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        
        $task = Task::where('id', $id)->with('taskTimes.notificationMethods')->first();
        $notificationMethods = NotificationMethod::get();

        return view('tasks.edit', compact('task', 'notificationMethods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        $request->validate([
            'name' => 'required|max:255',
            'start_time' => 'date_format:"Y-m-d H:i"|required|before:end_time',
            'end_time' => 'date_format:"Y-m-d H:i"|required|after:start_time',
            'notificationDateTime.*' => 'date_format:"Y-m-d H:i|nullable|after:today',
            'notificationMethod.*' => 'exists:notification_methods,id'
        ]);

        $task = Task::find($id);
        $task->name = $request->get('name');
        $task->description = $request->get('description');
        $task->start_time = $request->get('start_time');
        $task->end_time = $request->get('end_time');
        $task->save();

        $task->taskTimes()->delete();

        foreach($request->notificationDateTime as $key => $notificationDateTime){

            if(!$notificationDateTime) continue;

            $taskTime = $task->taskTimes()->save(
                new TaskTime([
                    'task_id' => $task->id,
                    'time' => $notificationDateTime
                ])
            );

            if($notificationMethod = $request->notificationMethod[$key]){

                $taskTime->notificationMethods()->attach($notificationMethod);
            };
        }

        return redirect('tasks')->with('success','Task succefully edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){

        $task = Task::where('id', $id)->delete();

        return redirect('tasks')->with('success','Task succefully deleted');
    }
}
