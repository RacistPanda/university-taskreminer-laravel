<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationMethod extends Model
{
    protected $fillable = [
    	'name',
    ];

    public function taskTimes(){

    	return $this->belongsToMany('App\TaskTime', 'task_time_notification_method');
    }
}
