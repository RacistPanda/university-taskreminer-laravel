<?php

namespace App\Traits;

use Log;

//read more about api
//https://www.lightsms.com/external/client/api/

trait SmsSendingTraits{

	private $phone;
	private $message;
	private $sender;
	private $params;
	private $apiKey;
	private $smsSignature;

	public function SmsSend(){

		$this
			->setParams()
			->setApiKey()
			->setSignature();

		if(!config('lightSms.SmsSend')) return 'Message not sent - change SMS_SEND to true in config';

		$answer = file_get_contents("https://www.lightsms.com/external/get/send.php?login={$this->params['login']}&signature={$this->smsSignature}&phone={$this->params['phone']}&text={$this->params['text']}&sender={$this->params['sender']}&timestamp={$this->params['timestamp']}");

		$result = json_decode($answer);

		if(isset($result->error)) 
			Log::error('SMS ERROR:' . $answer . ' check https://www.lightsms.com/external/client/api/ for error codes');

		return $answer;
	}

	public function SmsTo($phone){
		$phone = str_replace('+', '', $phone);
		$this->phone = $phone;

		return $this;
	}

	public function SmsMessage($message){

		$this->message = $message;

		return $this;
	}

	private function setSignature(){

		ksort( $this->params );
    	reset( $this->params );

    	$this->smsSignature = md5( implode( $this->params ) . $this->apiKey );
 
    	return $this;
	}

	private function setParams(){

		$this->params = [
			'login' => config('lightSms.SmsLogin'),
		    'phone' => $this->phone,
		    'sender' => config('lightSms.SmsSender'),
		    'text' => $this->message,
		    'timestamp' => time(),
		];

		return $this;
	}

	private function setApiKey(){

		$this->apiKey = config('lightSms.SmsApiKey');

		return $this;
	}

}