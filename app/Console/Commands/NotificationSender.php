<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\TaskTime;
use App\Task;

use App\Mail\TaskReminder;

use App\Traits\SmsSendingTraits;

use Mail;

class NotificationSender extends Command{

    use SmsSendingTraits;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notificationsender';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks if task notifications need to be sent.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(){

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        date_default_timezone_set('Europe/Vilnius');
        $taskTimes = TaskTime::where('time', '<=', date('Y-m-d H:i'))->where('notified', '0')->get();

        foreach($taskTimes as $taskTime){

            $notifications = $taskTime->notificationMethods()->get();

            $task = Task::find($taskTime->task_id);
            $user = $task->user()->first();

            foreach($notifications as $notification){
                
                if($notification->type == 'sms'){
                    if($user->phone){
                        $this
                            ->SmsTo($user->phone)
                            ->SmsMessage("Notification for '{$task->name}' which starts '{$task->start_time}'")
                            ->SmsSend();
                    }
                } else if($notification->type == 'email'){
                    
                    Mail::to($user->email)->send(new TaskReminder($task));
                }
            }

            $taskTime->notified = 1;
            $taskTime->save();
        }
    }
}
